class Board

  attr_reader :grid

  def initialize(grid = Array.new(3) { Array.new(3, nil) } )
    @grid = grid
  end

  def place_mark(pos, mark)
    self[pos] = mark if empty?(pos)
  end

  def empty?(pos)
    @grid[pos[0].to_i][pos[-1].to_i].nil?
  end

  def winner
    [:O, :X].each do |token|
      return token if triple?(token)
    end

    return nil
  end

  def over?
    winner || tie?
  end

  def tie?
    full? && !winner
  end

  private

  def triple?(token)
    @grid.any? do |row|
      return true if row.all? { |mark| mark == token }
    end

    @grid.transpose.any? do |col|
      return true if col.all? { |mark| mark == token }
    end

    diagonals(@grid).any? do |diag|
      return true if diag.all? { |mark| mark == token }
    end

    return nil
  end

  def diagonals(array)
    left = (0...array.length).map do |num|
      array[num][num]
    end

    right = (0...array.length).map do |num|
      array[num][array.length-1-num]
    end

    [left,right]
  end

  def full?
    @grid.flatten.none? { |mark| mark == nil }
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

end
