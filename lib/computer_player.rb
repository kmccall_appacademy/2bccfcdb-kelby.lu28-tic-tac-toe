class ComputerPlayer

  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board

    p @board.grid[0]
    p @board.grid[1]
    p @board.grid[2]
  end

  def get_move
    available_moves = []

    (0...@board.grid.length).each do |row|
      (0...@board.grid.length).each do |col|
        move = [row, col]
        available_moves << move if @board.empty?(move)
      end
    end

    available_moves.each do |move|
      return move if wins?(move)
    end

    return available_moves.sample
  end

  private

  def wins?(pos)
    row, col = pos
    @board.place_mark(pos, @mark)
    if @board.winner
      @board.grid[row][col] = nil
      return true
    else
      @board.grid[row][col] = nil
      return false
    end
  end

end
