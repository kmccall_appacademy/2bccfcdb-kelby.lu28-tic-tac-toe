require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_reader :player_one, :player_two, :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
    @mark = :X
  end

  def play_turn

    @current_player.display(@board)

    puts "---"
    while true
      move = @current_player.get_move
      if @board.empty?(move)
        @board.place_mark(move, @mark)
        break
      else
        next
      end
    end
    puts "#{@current_player.name} has made their move to #{move}."
    puts "---"

    switch_players!
  end

  def switch_players!
    @current_player =
    @current_player == @player_one ? @player_two : @player_one

    @mark = @mark == :O ? :X : :O
  end

  def play

    until @board.over?
      play_turn
    end

    @current_player.display(@board)
    conclude

  end

  private

  def conclude
    if @board.winner
      puts "Congratulations, #{@board.winner} wins the round!"
    else
      puts "The game was tied!"
    end
  end

end

# if __FILE__ == $PROGRAM_NAME
#   player1 = HumanPlayer.new("Bob")
#   player2 = HumanPlayer.new("Chris")
#
#   game = Game.new(player1, player2)
#   game.play
# end
