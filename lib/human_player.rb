class HumanPlayer

  attr_reader :name

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    p board.grid[0]
    p board.grid[1]
    p board.grid[2]
  end

  def get_move
    puts "#{@name}, where would you like to go?"
    input = gets.chomp
    move = [input[0].to_i, input[-1].to_i]

    if valid(move)
      return move
    else
      puts "That move is unavailable. Please enter a different move."
      get_move
    end

  end

  private

  def valid(move)
    row, col = move
    (0..2).include?(row) && (0..2).include?(col)
  end

end
